require "canvas"

Gamestate = require "lib/hump/gamestate"
Menu = require "states/menu"

highscore = 0 -- used by menu

function love.load()
   highscore = love.filesystem.read("highscore.save")
   if highscore == nil then
      love.filesystem.write("highscore.save", 0)
      highscore = 0
   else
      highscore = tonumber(highscore)
   end

   love.window.setMode(mainCanvasW*2, mainCanvasH*2, {
      resizable = true,
      minwidth = mainCanvasW,
      minheight = mainCanvasH,
   })
   love.graphics.setBackgroundColor(0, 0, 0)
   mainCanvas:setFilter("nearest", "nearest", 0)

   Gamestate.registerEvents()
   Gamestate.switch(Menu)
end
