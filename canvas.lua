mainCanvasW = 320
mainCanvasH = 384
mainCanvas = love.graphics.newCanvas(mainCanvasW, mainCanvasH)

function blit_main_canvas()
   local windowW, windowH = love.graphics.getDimensions()
   local mainCanvasScale = math.floor(math.min(
      windowW / mainCanvasW,
      windowH / mainCanvasH
   ))

   love.graphics.draw(
      mainCanvas,
      (windowW - mainCanvasW*mainCanvasScale) / 2,
      (windowH - mainCanvasH*mainCanvasScale) / 2,
      0,
      mainCanvasScale,
      mainCanvasScale
   )
end
