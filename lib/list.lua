return function(table)
  if #table == 0 then return nil end

  local list = { val = table[1], next = nil }
  local tip = list

  for i = 1, #table do
    local elem = { val = table[i], next = nil }
    tip.next = elem
    tip = elem
  end

  return list
end
