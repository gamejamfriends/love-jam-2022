# Destructive Interference

You are a golem tasked with saving this city. It's an impossible job! You must
feel the vibrations coming up from deep underground and... punch them! Slam
them! By making your own equal but opposite earthquake you can maintain the
integrity of the city and allow your squishy human friends (and their pets) to
escape!

Face the direction of oncoming waves and punch (small effect) or slam (big
effect) to generate waves to interfere with the earthquake. Face downwards to
generate waves on both sides. That's it!

## Löve Jam 2022

This game was hacked together in a week for Löve Jam 2022 by people who,
frankly, had other responsibilities to be taking care of. It was a wild ride.

## How to run

Just open with Löve2D.

## Controls

A joystick is recommended, but the keyboard is totally useable.

```
Keyboard wasd / arrow keys / Joystick d-pad = Change direction
Keyboard z or Joystick a                    = Punch (make small wave)
Keyboard x or Joystick b                    = Slam (make big wave)
```

## Assets

All art, music and sfx was created for the game during the Jam week.
