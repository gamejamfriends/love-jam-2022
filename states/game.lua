local list = require "./lib/list"
local gameover = require "states/gameover"

local anim8 = require "./lib/anim8"
local background = love.graphics.newImage("sprites/background.png")
local citySpr = love.graphics.newImage("sprites/city.png")
local cityGrid = anim8.newGrid(320, 96, 320, 768)
local cityAnim = anim8.newAnimation(cityGrid(1, '1-8'), 1)
cityAnim:gotoFrame(1)
cityAnim:pause()

-- player animations
local playerSpr = love.graphics.newImage("sprites/player.png")
local playerGrid = anim8.newGrid(64, 64, playerSpr:getWidth(), playerSpr:getHeight())
local playerAnims = {}
-- idle
playerAnims.idleDown = anim8.newAnimation(playerGrid(1, 2), 1)
playerAnims.idleRight = anim8.newAnimation(playerGrid(1, 3), 1)
playerAnims.idleLeft = playerAnims.idleRight:clone():flipH()
-- punch
playerAnims.punchDown = anim8.newAnimation(playerGrid('2-6', 2),{1/30, 1/30, 0.5, 1/30, 1/30})
playerAnims.punchRight = anim8.newAnimation(playerGrid('2-6', 3),{1/30, 1/30, 0.5, 1/30, 1/30})
playerAnims.punchLeft = playerAnims.punchRight:clone():flipH()
playerAnim = playerAnims.idleDown
--slam
playerAnims.slamDown = anim8.newAnimation(playerGrid('7-11', 2),{1/30, 1/30, 0.5, 1/30, 1/30})
playerAnims.slamRight = anim8.newAnimation(playerGrid('7-11', 3),{1/30, 1/30, 0.5, 1/30, 1/30})
playerAnims.slamLeft = playerAnims.slamRight:clone():flipH()

local baton = require "./lib/baton"
local input = baton.new{
   controls = {
      left = {'key:left', 'key:a', 'axis:leftx-', 'button:dpleft'},
      right = {'key:right', 'key:d', 'axis:leftx+', 'button:dpright'},
      up = {'key:up', 'key:w', 'axis:lefty-', 'button:dpup'},
      down = {'key:down', 'key:s', 'axis:lefty+', 'button:dpdown'},
      action1 = {'key:z', 'button:a'},
      action2 = {'key:x', 'button:b'},
      exit = {'key:escape', 'button:start'},
   },
   pairs = {
      move = {'left', 'right', 'up', 'down'}
   },
   joystick = love.joystick.getJoysticks()[1],
}

local player = {direction = 'down', state='idle', locktimer = 10} --locktimer set >0 to stop auto-attacking as soon as we load the game state

local secondcounter = 0

local MAXHEALTH = 300000
local HEALTHSTEP = MAXHEALTH / 8
local PIXPERBEAT = 16
local PUNCHIDLETIMER = 36

local font = love.graphics.setNewFont("terminus.ttf", 12)

-------------------
-- music library --
-------------------
--
local music = {
   count = 0,
   track1 = love.audio.newSource("music/120.ogg", "static"),
   bpm1 = 120,
   track2 = love.audio.newSource("music/150.ogg", "static"),
   bpm2 = 150,
   track3 = love.audio.newSource("music/180.ogg", "static"),
   bpm3 = 180,
}
local sfx = {
   punch = love.audio.newSource("sfx/punch.ogg", "static"),
   slam = love.audio.newSource("sfx/slam.ogg", "static"),
   quake = love.audio.newSource("sfx/quake.wav", "static"),
}

--------------------------
-- other game functions --
--------------------------

local pulses = {
   small        = list({ 5, 9, 12, 13, 12, 9, 5, 0, -2, -4, -5, -6, -5, -4, -2}),
   inverseSmall = list({ -5, -9, -12, -13, -12, -9, -5, 0, 2, 4, 5, 6, 5, 4, 2}),
   large        = list({ 10, 19, 25, 28, 25, 19, 10, 0, -5, -9, -13, -16, -13, -9, -5, 0, 3, 6, 8, 6, 3}),
   inverseLarge = list({ -10, -19, -25, -28, -25, -19, -10, 0, 5, 9, 13, 16, 13, 9, 5, 0, -3, -6, -8, -6, -3}),
}

local function genPulses()
   local left = nil
   local right = nil
   local position = love.math.random(7)

   if position == 1 then left = pulses.small end
   if position == 2 then right = pulses.inverseSmall end
   if position == 3 then
      left = pulses.small
      right = pulses.inverseSmall
   end
   if position == 4 then left = pulses.large end
   if position == 5 then right = pulses.inverseLarge end
   if position == 6 then
      left = pulses.large
      right = pulses.inverseLarge
   end

   return left, right
end

----------------
-- game class --
----------------

local Game = {}
Game.__index = Game

function Game.new()
   local game = setmetatable({}, Game)

   game.health = MAXHEALTH
   game.score = 0
   game.BPM = 120
   game.moveRemainder = 0
   game.pxPerSec = PIXPERBEAT
   game.leftP = {}
   game.leftS = {}
   game.leftPBuf = nil
   game.leftSBuf = nil
   game.leftTop = {}
   game.rightS = {}
   game.rightP = {}
   game.rightPBuf = nil
   game.rightSBuf = nil
   game.rightTop = {}
   game.scrollPos = -1
   game.shudderFrames = 0

   for i = 1, 220 do
      game.leftP[i] = 0
      game.leftS[i] = 0
      game.rightS[i] = 0
      game.rightP[i] = 0
   end
   for i = 1, 64 do
      game.leftTop[i] = 0
      game.rightTop[i] = 0
   end

   return game
end

function Game:update(dt)
   if self.health <= 0 then
      Gamestate.switch(gameover.new(highscore, self.score))

      if self.score > highscore then
         highscore = self.score
         love.filesystem.write("highscore.save", tostring(self.score))
      end
      return
   end

   -- play a track
   if music.count == 0 then
      if not music.track1:isPlaying() then
         love.audio.stop()
         love.audio.play(music.track1)
      end
      music.count = 1
      self.BPM = music.bpm1
      self.pxPerSec = PIXPERBEAT/120 * self.BPM
   elseif music.count == 1 then
      if not music.track1:isPlaying() then
         love.audio.play(music.track2)
         music.count = 2
         self.BPM = music.bpm2
         self.pxPerSec = PIXPERBEAT/120 * self.BPM
      end
   elseif music.count == 2 then
      if not music.track2:isPlaying() then
         love.audio.play(music.track3)
         music.count = 3
         self.BPM = music.bpm3
         self.pxPerSec = PIXPERBEAT/120 * self.BPM
      end
   else
      if not music.track3:isPlaying() then love.audio.play(music.track3) end
   end

   input:update(dt)

   if input:down('exit') then
      love.event.quit()
      return
   end

   -- if you are not locked into an attack
   if player.locktimer == 0 then
      player.state = 'idle'
      -- Change Directions
      if input:down('left') then
         if player.direction ~= 'left' then
            player.direction = 'left'
         end
      end
      if input:down('right') then
         if player.direction ~= 'right' then

            player.direction = 'right'
         end
      end

      if input:down('down') or input:pressed('up') then
         if player.direction ~= 'down' then
            playerAnim = playerAnims.idleDown
            player.direction = 'down'
            player.state = 'idle'
         end
      end
      if input:pressed('action1') then
         sfx.punch:play()
         player.state = 'punch'
         player.locktimer = PUNCHIDLETIMER

         if player.direction == 'left' or player.direction == 'down' then
            self.leftSBuf = pulses.inverseSmall
         end
         if player.direction == 'right' or player.direction == 'down' then
            self.rightSBuf = pulses.small
         end
      end
      if input:pressed('action2') then
         sfx.slam:play()
         player.state = 'slam'
         player.locktimer = PUNCHIDLETIMER

         if player.direction == 'left' or player.direction == 'down' then
            self.leftSBuf = pulses.inverseLarge
         end
         if player.direction == 'right' or player.direction == 'down' then
            self.rightSBuf = pulses.large
         end
      end
   else
      player.locktimer = player.locktimer - 1
   end

   if player.state == 'punch' then
      if player.direction == 'left' then
         playerAnim = playerAnims.punchLeft
      elseif player.direction == 'right' then
         playerAnim = playerAnims.punchRight
      elseif player.direction == 'down' then
         playerAnim = playerAnims.punchDown
      end
   elseif player.state == 'slam' then
      if player.direction == 'left' then
         playerAnim = playerAnims.slamLeft
      elseif player.direction == 'right' then
         playerAnim = playerAnims.slamRight
      elseif player.direction == 'down' then
         playerAnim = playerAnims.slamDown
      end
   else
      if player.direction == 'left' then
         playerAnim = playerAnims.idleLeft
      elseif player.direction == 'right' then
         playerAnim = playerAnims.idleRight
      elseif player.direction == 'down' then
         playerAnim = playerAnims.idleDown
      end
   end

   playerAnim:update(dt)

   -- move seizmic lines
   love.graphics.setLineStyle("rough")
   local mvAmount = math.floor((dt + self.moveRemainder) * self.pxPerSec)
   if mvAmount > 64 then mvAmount = 64 end -- we're in trouble with framerate

   if self.shudderFrames > 0 then
      self.shudderFrames = self.shudderFrames - mvAmount
      if not sfx.quake:isPlaying() then
         sfx.quake:play()
      end
   end

   if mvAmount > 0 then
      self.moveRemainder = self.moveRemainder + dt - mvAmount/self.pxPerSec

      -- determine damage for points we're going to remove
      local damage = 0
      for i = 1, 64-mvAmount do
         damage = damage + math.abs(self.leftTop[i])
         damage = damage + math.abs(self.rightTop[i])
      end
      self.health = self.health - damage
      local damageFrame = math.min(9 - math.ceil(self.health / HEALTHSTEP), 8)
      cityAnim:gotoFrame(damageFrame)

      if damage/mvAmount > 200 then
         self.shudderFrames = 32
      end

      -- shift top up
      for i = 1, 65-mvAmount do
         self.leftTop[i] = self.leftTop[i+mvAmount]
         self.rightTop[i] = self.rightTop[i+mvAmount]
      end

      -- update the end of the top line with the combined P/S lines
      for i = 1, mvAmount do
         self.leftTop[64 - mvAmount + i] = self.leftP[i] + self.leftS[i]
         self.rightTop[64 - mvAmount + i] = self.rightP[i] + self.rightS[i]
      end

      -- move left/right P/S up
      for i = 1, 220-mvAmount do
         self.leftP[i] = self.leftP[i+mvAmount]
         self.leftS[i] = self.leftS[i+mvAmount]
         self.rightP[i] = self.rightP[i+mvAmount]
         self.rightS[i] = self.rightS[i+mvAmount]
      end

      -- generate tail of left/right P/S lines
      for i = 221-mvAmount, 220 do
         self.scrollPos = self.scrollPos + 1
         if self.scrollPos % 32 == 0 then
            self.leftPBuf, self.rightPBuf = genPulses()
         end

         if self.leftPBuf == nil then
            self.leftP[i] = 0
         else
            self.leftP[i] = self.leftPBuf.val
            self.leftPBuf = self.leftPBuf.next
         end
         if self.rightPBuf == nil then
            self.rightP[i] = 0
         else
            self.rightP[i] = self.rightPBuf.val
            self.rightPBuf = self.rightPBuf.next
         end

         self.leftS[i] = 0
         self.rightS[i] = 0
         if self.leftSBuf ~= nil then
            self.leftS[i-160] = self.leftSBuf.val
            self.leftSBuf = self.leftSBuf.next
         end
         if self.rightSBuf ~= nil then
            self.rightS[i-160] = self.rightSBuf.val
            self.rightSBuf = self.rightSBuf.next
         end
      end
   else
      self.moveRemainder = self.moveRemainder + dt
   end

   cityAnim:update(dt)

   --tick up score every second
   if secondcounter < 1 then
      secondcounter = secondcounter + dt
   else
      secondcounter = 0
      self.score = self.score + self.BPM/10
   end

end

function Game:draw()
   love.graphics.setCanvas(mainCanvas)

   love.graphics.draw(background, 0, 96)

   local cityX = 0
   if self.shudderFrames > 0 then cityX = love.math.random(8) - 4 end
   cityAnim:draw(citySpr, cityX, 0)

   playerAnim:draw(playerSpr, 128, 192)

   -- top seizmic lines
   for i = 2, 63 do
      local prevLX = 64 + self.leftTop[i-1]
      local lx = 64 + self.leftTop[i]
      love.graphics.line(prevLX, 95+i, lx+1, 96+i)

      local prevRX = 256 + self.rightTop[i-1]
      local rx = 256 + self.rightTop[i]
      love.graphics.line(prevRX, 95+i, rx+1, 96+i)
   end

   -- side seizmic lines
   for i = 2, 215 do
      local prevLP = 32 + self.leftP[i-1]
      local lP = 32 + self.leftP[i]
      local prevLS = 96 + self.leftS[i-1]
      local lS = 96 + self.leftS[i]
      love.graphics.line(prevLP, 168+i, lP+1, 169+i)
      love.graphics.line(prevLS, 168+i, lS+1, 169+i)

      local prevRP = 288 + self.rightP[i-1]
      local rP = 288 + self.rightP[i]
      local prevRS = 224 + self.rightS[i-1]
      local rS = 224 + self.rightS[i]
      love.graphics.line(prevRP, 168+i, rP+1, 169+i)
      love.graphics.line(prevRS, 168+i, rS+1, 169+i)
   end

   -- HUD
   love.graphics.setColor(0, 0, 0, 1)
   love.graphics.rectangle(
      "fill",
      3,
      3,
      self.health/MAXHEALTH*(mainCanvasW-6),
      3
   )

   love.graphics.setColor(1,1,1,1)
   love.graphics.print(
      "Score",
      (mainCanvasW - font:getWidth("Score"))/2,
      324
   )
   love.graphics.print(
      tostring(self.score),
      (mainCanvasW - font:getWidth(tostring(self.score)))/2,
      336
   )
   love.graphics.print(
      "BPM",
      (mainCanvasW - font:getWidth("BPM"))/2,
      356
   )
   love.graphics.print(
      tostring(self.BPM),
      (mainCanvasW - font:getWidth(tostring(self.BPM)))/2,
      368
   )

   love.graphics.setColor(1, 1, 1, 1)
   love.graphics.setCanvas()
   blit_main_canvas()
end

return Game
