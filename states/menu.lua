local menu = {}

local game = require "states/game"

local baton = require "./lib/baton"
local input = baton.new{
   controls = {
      exit = {'key:escape'},
      start = {'key:z', 'button:a', 'key:space', 'key:x', 'button:b'},
   },
   pairs = {},
   joystick = love.joystick.getJoysticks()[1],
}

local music = love.audio.newSource("music/menu.ogg", "stream")
local background = love.graphics.newImage("sprites/menu-background.png")
local font = love.graphics.setNewFont("terminus.ttf", 12)
local fontHuge = love.graphics.setNewFont("terminus.ttf", 24)

function menu:update(dt)
   input:update()
   if not music:isPlaying() then
      love.audio.stop()
      love.audio.play(music)
   end

   if input:pressed('exit') then
      love.event.quit()
   elseif input:pressed('start') then
      Gamestate.switch(game.new())
   end

end

function menu:draw()
   local lineHeight = 12

   love.graphics.setCanvas(mainCanvas)

   love.graphics.setColor(1, 1, 1, 1)
   love.graphics.draw(background, 0, 0)

   -- TOP

   love.graphics.setColor(0, 1, 0, 1)
   love.graphics.setFont(fontHuge)
   love.graphics.print(
      "DESTRUCTIVE",
      (mainCanvasW - fontHuge:getWidth("DESTRUCTIVE"))/2,
      20
   )
   love.graphics.print(
      "INTERFERENCE",
      (mainCanvasW - fontHuge:getWidth("INTERFERENCE"))/2,
      40
   )

   love.graphics.setColor(1, 1, 1, 1)
   love.graphics.setFont(font)
   local highscoreStr = "High Score: "..tostring(highscore)
   love.graphics.print(
      highscoreStr,
      (mainCanvasW - font:getWidth(highscoreStr))/2,
      80
   )

   -- MIDDLE
   local middleBase = 150

   local description1 = "BEAT THE EARTHQUAKE INTO SUBMISSION!"
   local description2 = "CREATE AN ANTI-EARTHQUAKE!"
   love.graphics.print(
      description1,
      (mainCanvasW - font:getWidth(description1))/2,
      middleBase
   )
   love.graphics.print(
      description2,
      (mainCanvasW - font:getWidth(description2))/2,
      middleBase + lineHeight
   )

   love.graphics.print(
      "WASD / ARROW KEYS / JOYSTICK:",
      64,
      middleBase + lineHeight*2.5
   )
   love.graphics.print(
      "    SET FACING",
      64,
      middleBase + lineHeight*3.5
   )
   love.graphics.print(
      "Z OR JOYSTICK 1: PUNCH!",
      64,
      middleBase + lineHeight*5
   )
   love.graphics.print(
      "X OR JOYSTICK 2: SLAM!",
      64,
      middleBase + lineHeight*6.5
   )

   -- BOTTOM

   local pressStartBase = 276
   local startStr1 = "PRESS <SPACE>, <Z>, OR A"
   love.graphics.print(
      startStr1,
      (mainCanvasW - font:getWidth(startStr1))/2,
      pressStartBase
   )
   local startStr2 = "JOYSTICK BUTTON TO START"
   love.graphics.print(
      startStr2,
      (mainCanvasW - font:getWidth(startStr2))/2,
      pressStartBase + lineHeight
   )

   love.graphics.setCanvas()
   blit_main_canvas()
end

return menu
