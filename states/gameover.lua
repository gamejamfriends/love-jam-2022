local Gameover = {}
Gameover.__index = Gameover

local baton = require "./lib/baton"
local input = baton.new{
   controls = {
      exit = {'key:escape'},
   },
   pairs = {},
   joystick = love.joystick.getJoysticks()[1],
}

local music = love.audio.newSource("music/gameover.ogg", "stream")
local background = love.graphics.newImage("sprites/menu-background.png")
local font = love.graphics.setNewFont("terminus.ttf", 12)
local fontHuge = love.graphics.setNewFont("terminus.ttf", 24)

function Gameover:update(dt)
   input:update()
   if not self.musicPlayed and not music:isPlaying() then
      love.audio.stop()
      love.audio.play(music)
      self.musicPlayed = true
   end

   if input:pressed('exit') then
      love.event.quit()
   end
end

function Gameover:draw()
   local lineHeight = 12

   love.graphics.setCanvas(mainCanvas)

   love.graphics.setColor(1, 0, 0, 1)
   love.graphics.draw(background, 0, 0)

   -- TOP

   love.graphics.setFont(fontHuge)
   love.graphics.print(
      "GAME",
      (mainCanvasW - fontHuge:getWidth("GAME"))/2,
      20
   )
   love.graphics.print(
      "OVER",
      (mainCanvasW - fontHuge:getWidth("GAME"))/2,
      40
   )

   love.graphics.setColor(1, 1, 1, 1)
   love.graphics.setFont(font)
   if self.score > self.prevHighscore then
      love.graphics.setColor(0, 1, 0, 1)
      local highscoreStr = "High Score: "..tostring(self.score)
      love.graphics.print(
         highscoreStr,
         (mainCanvasW - font:getWidth(highscoreStr))/2,
         80
      )
   else
      love.graphics.setColor(1, 1, 1, 1)
      local highscoreStr = "High Score: "..tostring(self.prevHighscore)
      love.graphics.print(
         highscoreStr,
         (mainCanvasW - font:getWidth(highscoreStr))/2,
         80
      )
   end

   -- MIDDLE

   if self.score > self.prevHighscore then
      love.graphics.setColor(0, 1, 0, 1)
      local highscoreStr = "** NEW HIGH SCORE **"
      love.graphics.print(
         highscoreStr,
         (mainCanvasW - font:getWidth(highscoreStr))/2,
         180
      )
   else
      love.graphics.setColor(1, 1, 1, 1)
      local highscoreStr = "Your Score: "..tostring(self.score)
      love.graphics.print(
         highscoreStr,
         (mainCanvasW - font:getWidth(highscoreStr))/2,
         180
      )
   end

   -- BOTTOM

   love.graphics.setColor(1, 1, 1, 1)
   local pressStartBase = 276
   local exitStr1 = "PRESS <ESCAPE> TO EXIT"
   love.graphics.print(
      exitStr1,
      (mainCanvasW - font:getWidth(exitStr1))/2,
      pressStartBase
   )

   love.graphics.setCanvas()
   blit_main_canvas()
end

function Gameover.new(prevHighscore, score)
   local gameover = setmetatable({}, Gameover)
   gameover.prevHighscore = prevHighscore
   gameover.score = score
   gameover.musicPlayed = false
   return gameover
end

return Gameover
